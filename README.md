# README #

Project based on the new cli version for vue (3)

### What will i learn? ###

* Installation
* Working with new changes (!= V2):
    1. Vue.createApp() to initialize the vue project/instance

    2. el: '#app', element on app.js will never be used to create an instance using vue 3

    3.  We must use -from suffix in style classes to create CSS animations

* Working with new features:
    1. Teleports: You are able to move your html template to a specific place in the dom, like "body" , see ConfirmDialog.vue

    2. Fragments:  You are able to set multiple elements/tags into your html template without using an unnecessary wrapper like DIV. Using  Fragment unlike to  vue2 you don't need to group/wrap all elements ( to use DIV is avoided) . see ConfirmDialog.vue

### how it runs? ###
* click on index.html per folder and you will see the results
* In case of router-v3 and vuex-v3 folders we need to go there and make:
    1. npm i
    2. npm run serve

### What's new?:
![readme.png](readme.png)

### New Features!:
![readme-features](readme-features.png)
