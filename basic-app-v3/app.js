const app = Vue.createApp({ // vue 3 you need to use this Vue.createApp to create a Vue project instance
  data() { // vue 3 data must be method.. returning its  values
    return { message: 'This works in Vue 2!' };
  },
  methods: {
    changeMessage() {
      this.message = 'Will it work in Vue 3?';
    },
  },
});

app.component('the-button', {
  emits: ['update'], // setting the emit event on the component
  template: '<button @click="updateMessage">Click me</button>',
  methods: {
    updateMessage() {
      this.$emit('update');
    },
  },
});

app.mount('#app');
