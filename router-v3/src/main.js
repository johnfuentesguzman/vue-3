import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import App from './App.vue'; // Bringing app instance
import WelcomeScreen from './pages/WelcomeScreen.vue';
import UsersList from './pages/UsersList.vue';

//  Adding Router
const router = createRouter({
  history: createWebHistory(), // new in vue 3
  routes: [ // in app.vue  (html) router-view tag must wrap the transiction tag/effect (if its neccesary) and project components
    { path: '/', component: WelcomeScreen },
    { path: '/users', component: UsersList },
  ],
});

const app = createApp(App);

app.use(router);

router.isReady().then(() => { // using promisse to check to mount the app just when the routers/links have been loaded
  app.mount('#app');
});
